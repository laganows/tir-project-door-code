#!/usr/bin/env python

# ----- BEGIN INITIALIZATION -----
import os
from serial import Serial
import sys
import time
import json
import gspread
from oauth2client.client import SignedJwtAssertionCredentials

# get access to spreadsheet
json_key = json.load(open('iot-first-fifth-cbc92bb37a1f.json'))
scope = ['https://spreadsheets.google.com/feeds']
credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'].encode(), scope)
gc = gspread.authorize(credentials)

# Open a worksheet from spreadsheet with one shot
wks = gc.open("code").sheet1

serial = Serial('/dev/ttyS0', 38400, timeout=1)


# ----- END INITIALIZATION -----

# function that sets user depending on knob's position
def setUser(ch):
    global userSelected
    if ch < 21:
        userSelected = 'admin'
        setRGB(64 + 32 + 16)
    elif ch < 42:
        userSelected = 'locator'
        setRGB(64 + 8 + 4)
    elif ch < 63:
        userSelected = 'cleaner'
        setRGB(64 + 2 + 1)
    else:
        userSelected = ''
        setRGB(64)


# function that turns led on
def turnLedOn():
    serial.write(chr(33))


# function that turns led off
def turnLedOff():
    serial.write(chr(32))


# function that sets colour of RGB led
def setRGB(val):
    serial.write(chr(val))


# function that opes the door - motion of servo
def openDoor():
    serial.write(chr(30))
    print "INFO: Door open."


# function that closes the door - motion of servo and resetting machine to initial state
def closeDoor():
    serial.write(chr(1))
    turnLedOff()
    setRGB(64)
    global phase
    phase = 0
    global userSelected
    userSelected = ''
    print "INFO: Door closed."


# function that checks correct of entered code
def checkCode():
    return enteredCode == userCodes[userSelected]


# serial init
serial.write(chr(128 + 16 + 8 + 4 + 1))
# variable that represents current user
# available values: admin; locator; cleaner; ''
userSelected = ''
# current phase
# available values: 0 - 4
phase = 0
# dictionary of enter codes
# data is read from spreadsheet
userCodes = {'admin': wks.acell("A1").value.split(","),
             'locator': wks.acell("B1").value.split(","),
             'cleaner': wks.acell("C1").value.split(",")}
# list containing digits of entered code
enteredCode = []
# user whose code is changed
programmingUser = 0

# setting initial state
closeDoor()
turnLedOff()
setRGB(64)
print "INFO: Initialization finished."

try:
    while True:
        cc = serial.read(1)
        if len(cc) > 0:
            ch = int(ord(cc))
            if phase == 0:  # initial state
                if 63 < ch < 128:  # setting user depending on knob's position
                    setUser(ch - 64)
                elif ch == 195 or ch == 197:  # user confirmation
                    phase = 1
                    turnLedOn()
                    enteredCode = []
                    print "INFO: User confirmed. User: ", userSelected, ". Enter your code and confirm with light."
            elif phase == 1:  # entering code
                if ch == 195:
                    enteredCode.append('1')
                elif ch == 197:
                    enteredCode.append('2')
                elif ch == 193:  # code confirmation with light
                    if checkCode():  # checking correct of entered code
                        openDoor()
                        phase = 2
                    else:  # if code is incorrect, set initial state
                        closeDoor()
            elif phase == 2:  # "inside"
                if ch == 197:  # quit with button2
                    closeDoor()
                elif ch == 195 and userSelected == 'admin':  # if admin pressed button1 - start programming mode
                    setRGB(64 + 32 + 16 + 8 + 4)
                    phase = 3
                    print "INFO: Entering programming mode by admin..."
            elif phase == 3:  # programming mode - user selection
                if ch == 195:  # counting hits of button1
                    programmingUser += 1
                elif ch == 197:  # confirmation with button2
                    if programmingUser < 1 or programmingUser > 3:  # checking correct
                        programmingUser = 0
                        setRGB(64)
                        phase = 2
                        print "INFO: No such user. Leaving programming mode..."
                    else:
                        if programmingUser == 1:
                            programmingUser = 'admin'
                        elif programmingUser == 2:
                            programmingUser = 'locator'
                        elif programmingUser == 3:
                            programmingUser = 'cleaner'
                        setRGB(64 + 8 + 4 + 2 + 1)
                        enteredCode = []
                        phase = 4
                        print "INFO: Selected programming for: ", programmingUser, ". Confirm with light."
            elif phase == 4:  # programming mode - entering new code
                if ch == 195:
                    enteredCode.append('1')
                elif ch == 197:
                    enteredCode.append('2')
                elif ch == 193:  # confirmation with light
                    userCodes[programmingUser] = enteredCode
                    print "INFO: User: ", programmingUser, ", entered code: ", enteredCode, ". Sending data to server..."
                    if programmingUser == 'admin':
                        wks.update_acell('A1', ','.join(enteredCode))
                    elif programmingUser == 'locator':
                        wks.update_acell('B1', ','.join(enteredCode))
                    elif programmingUser == 'cleaner':
                        wks.update_acell('C1', ','.join(enteredCode))
                    setRGB(64)
                    phase = 2  # go back to "inside"
                    print "INFO: Sending successful."
            else:
                pass
except KeyboardInterrupt:
    sys.exit()
